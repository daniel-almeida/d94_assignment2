package assignment2;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * 
 * University of Toronto (Winter 2014) - Daniel Almeida - danielalmeida.ti at
 * gmail.com
 * 
 * Please, note that most of the code is based on the user guide available at
 * http://netty.io/wiki/user-guide-for-4.x.html
 * 
 */

@Sharable
public class ServerHandler extends ChannelInboundHandlerAdapter {
	private ChannelFuture f;
	private int msgCount = 0;

	public void channelRead(ChannelHandlerContext ctx, Object msg)
			throws Exception {
		final ByteBuf sum = ctx.alloc().buffer(4);
		ByteBuf in = (ByteBuf) msg;
		int temp, i = 0;
		int total = 0;

		msgCount++;
		// The msgCount is initialized for each client connect.
		// If using more than 1 client, messages sent to the std ouput may have
		// the same count.
		System.out.println("\nMessage " + msgCount + "\n");
		while (in.isReadable()) {
			i++;
			// temp is used to avoid executing readByte more than once. This
			// would change the readerIndex n times
			temp = (int) in.readByte();
			total += temp;
			System.out.println("Byte " + i + ": " + temp);
		}
		System.out.println("Sum: " + total);
		sum.writeInt(total);
		f = ctx.writeAndFlush(sum);
	}

	@Override
	public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {

		// Must be careful about closing the channel.
		// If executed at the end of the readChannel, server doesn't receive new
		// messages from client
		f.addListener(ChannelFutureListener.CLOSE);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
	}
}

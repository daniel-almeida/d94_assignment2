package assignment2;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

/**
 * 
 * University of Toronto (Winter 2014) - Daniel Almeida - danielalmeida.ti at
 * gmail.com
 * 
 * Please, note that most of the code is based on the user guide available at
 * http://netty.io/wiki/user-guide-for-4.x.html
 * 
 */

public class ClientHandler extends ChannelInboundHandlerAdapter {

    public ClientHandler() {

	}

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) {
        ByteBuf m = (ByteBuf) msg; // (1)
        try {
            int sum = m.readInt();
            System.out.println("\nSum of the message sent is: " + sum);
        } finally {
            m.release();
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}

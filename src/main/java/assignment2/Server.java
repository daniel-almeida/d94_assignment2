package assignment2;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;

/**
 * 
 * University of Toronto (Winter 2014) - Daniel Almeida - danielalmeida.ti at
 * gmail.com
 * 
 * This Server expects that a client will connect and send a stream of bytes (ByteBuf). 
 * Each individual byte is accessed and its value add up and then sent back to the Client.
 * By default, it uses 8080 as the port for connection, but you can pass another value as an argument.
 * To make it easy to see its behavior, this server sends to the std output each byte received 
 * and also counts the messages received. Most of it is done in the class ServerHandler.
 * 
 * Please, note that most of the code is based on the user guide available at
 * http://netty.io/wiki/user-guide-for-4.x.html
 * 
 */
public class Server {

    private final int port;

    public Server(int port) {
        this.port = port;
    }

    public void run() throws Exception {
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
             .channel(NioServerSocketChannel.class)
             .option(ChannelOption.SO_BACKLOG, 100)
             .childOption(ChannelOption.SO_KEEPALIVE, true)
             .childHandler(new ChannelInitializer<SocketChannel>() {
                 @Override
                 public void initChannel(SocketChannel ch) throws Exception {
                     ch.pipeline().addLast(
                    		 // Server doesn't need encoding or decoding because it deals directly with bytes (ByteBuf)
                             new ServerHandler());
                 }
             });

            ChannelFuture f = b.bind(port).sync();
            System.out.println("[SERVER] Started!");

            f.channel().closeFuture().sync();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    public static void main(String[] args) throws Exception {
        int port;
        if (args.length > 0) {
            port = Integer.parseInt(args[0]);
        } else {
            port = 8080;
        }
        new Server(port).run();
    }
}

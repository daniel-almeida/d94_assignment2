package assignment2;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.string.StringEncoder;

/**
 * 
 * University of Toronto (Winter 2014) - Daniel Almeida - danielalmeida.ti at
 * gmail.com
 * 
 * This Client encodes a string read from the user and sends it to the server,
 * which will sum each byte of the string and send back the resulting integer.
 * The result received is sent to the std output. These features are implemented
 * in the class ClientHandler. The Client uses "localhost" as server hostname
 * and 8080 as default port for connection, but it's possible to pass both
 * values as arguments.
 * 
 * Please, note that most of the code is based on the user guide available at
 * http://netty.io/wiki/user-guide-for-4.x.html
 * 
 */

public class Client {

	private final String host;
	private final int port;

	public Client(String host, int port) {
		this.host = host;
		this.port = port;
	}

	public void run() throws Exception {

		EventLoopGroup group = new NioEventLoopGroup();
		try {
			Bootstrap b = new Bootstrap();
			b.group(group).channel(NioSocketChannel.class)
					.option(ChannelOption.TCP_NODELAY, true)
					.handler(new ChannelInitializer<SocketChannel>() {
						@Override
						public void initChannel(SocketChannel ch)
								throws Exception {
							ch.pipeline().addLast(
							// Because the Client reads a string from the user,
							// it's necessary to encode the string (serialize)
									new StringEncoder(), new ClientHandler());
						}
					});

			// Start the client.
			ChannelFuture f = b.connect(host, port).sync();
			BufferedReader in = new BufferedReader(new InputStreamReader(
					System.in));
			System.out.println("[CLIENT] Message: ");
			while (true) {
				String line = in.readLine();
				if (line == null) {
					break;
				}
				f.channel().writeAndFlush(line);
			}

			f.channel().closeFuture().sync();
		} finally {
			group.shutdownGracefully();
		}
	}

	public static void main(String[] args) throws Exception {

		if (args.length != 2 && args.length != 0) {
			System.err.println("Usage: " + Client.class.getSimpleName()
					+ " [<host> <port>]");
			return;
		}

		final String host;
		final int port;

		if (args.length == 0) {
			host = "localhost";
			port = 8080;
		} else {
			host = args[0];
			port = Integer.parseInt(args[1]);
		}

		new Client(host, port).run();
	}
}

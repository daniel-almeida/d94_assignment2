University of Toronto (Winter 2014)
Daniel Almeida - danielalmeida.ti at gmail.com
Feb 13 2014

"A client and a server using NETTY. The client sends N
messages each of N bytes, and the server's respond to each message by
sending out 4 bytes containing the sum of the N bytes sent by the
client."